const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BookSchema = new Schema({
  title: { type: String, required: true },
  author: String,
  email: String,
  category: String,
  price: { type: Number, required: true, default: 100 },
  datePublished: { type: Date, required: true },
  name: String,
  marks: Number,
  edition: [Number],
});

module.exports = mongoose.model('Book', BookSchema);
