const express = require('express');
const app = express();

const mongoose = require('mongoose');

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const Book = require('./model/bookSchema');

const db = 'mongodb://localhost:27017/crud';
mongoose
  .connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(
    () => {
      console.log('db is connected');
    },
    (err) => {
      console.log('err', err);
    }
  );

//create
app.post('/save', function (req, res) {
  const newBook = new Book({
    title: req.body.title,
    author: req.body.author,
    email: req.body.email,
    category: req.body.category,
    price: req.body.price,
    datePublished: new Date(req.body.datePublished),
    edition: [...req.body.edition],
  });

  newBook
    .save()
    .then((data) => {
      res.send('Data inserted');
    })
    .catch((err) => {
      res.send(err);
    });
});

//create 10000
app.post('/save10000', function (req, res) {
  for (let i = 1; i < 10000; i++) {
    var newBook = new Book({
      title: `myTitle${i}`,
      author: `myAuthor${i}`,
      email: `emp.${i}@yudiz.in`,
      category: `myCategory${i}`,
      price: 100 + i,
      datePublished: new Date(),
      edition: [1 + 1, i + 2, i + 3, i + 4, i + 5, i + 6, i + 7, i + 8, i + 9],
    });
    newBook.save(function (err, data) {
      if (err) {
        console.log(error);
      } else {
        console.log('inserted', i);
      }
    });
  }
  res.send('10000 data inserted');
});

//read
app.get('/find', function (req, res) {
  const email = req.body.email;
  Book.find({ email: email }, function (err, data) {
    if (err) {
      console.log(err);
    } else {
      res.send(data);
    }
  });
});
//read all
app.get('/findall', function (req, res) {
  Book.find(function (err, data) {
    if (err) {
      console.log(err);
    } else {
      res.send(data);
    }
  });
});

//update
app.put('/update1', (req, res) => {
  old_title = req.body.old_title;
  new_title = req.body.new_title;

  Book.updateOne(
    { title: old_title },
    {
      $set: {
        title: new_title,
      },
    }
  )
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send(err);
    });
});

//update : field operator
app.put('/update2', (req, res) => {
  title = req.body.title;

  Book.updateOne(
    { title: title },
    {
      // $currentDate: { datePublished: true }, //modifies value of field to current date
      // $inc: { price: -2, 'metrics.orders': 1 }, // Increments the value of the field by the specified amount.
      // $min: { price: 150 }, // update price field with price = min(price,150)
      // $max: { price: 300 }, // update price field with price = max(price,300)
      // $mul: { price: 2 }, // update price field with price = price * 2
      // $rename: { datePublished: 'dateOfPublication' }, //rename field name. Must be done in isolation else returm error
      // $set: { asc: 'anuj' }, // Replace the title value with anuj
      // $set: { name: 'anuj jha' },
      // $setOnInsert: { marks: 200 },
      $unset: { email: '', category: '' },
    },
    { strict: false, upsert: true }
  )
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send(err);
    });
});

//update : array operator
app.put('/update3', (req, res) => {
  title = req.body.title;

  Book.updateMany(
    { title: title /*, edition: 1 */ },
    {
      // $set: { 'edition.$': 100 }, // $ update one occurance
      // $set: { 'edition.$[]': 100 }, // $[] update all occurance of matched query //dout :it is updating all occurance instead of just matched one
      $push: { edition: 1000 },
      // $pop: { edition: 1 }, //removes from last position
      // $pop: { edition: -1 }, // removes from first position
      // $pull: { edition: 9 }, //remove all matched occurance from array
      // $pullAll: { edition: [8, 9] },
      //modifiers
      // $push: { edition: { $each: [111, 222, 333] } }, //it can be used with $push operator for inserting array values in the field array
      // $addToSet: { edition: { $each: [111, 222, 333, 55] } }, //it adds unqiue value to the set. if the same value is passed multiple times then it does nothing
      // $push: { edition: { $each: [3, 1, 5, 2, 7], $sort: 1 } },
      // $push: { edition: { $each: [10, 20, 30, 40, 50, 60, 70], $slice: 5 } },//it limits the number of array elements during push operation. (+5 : STARTING FROM START)
      // $push: { edition: { $each: [10, 20, 30, 40, 50, 60, 70], $slice: -5 } }, //it limits the number of array elements during push operation. (-5 : STARTING FROM END)
    },
    { upsert: true }
  )
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.send(err);
    });
});

//delete
app.delete('/delete', function (req, res) {
  const title = req.body.title;
  Book.remove({ title: title }, function (err, data) {
    if (err) {
      console.log(err);
    } else {
      res.send(data);
    }
  });
});

app.listen(3000, () => {
  console.log('server is up');
});
