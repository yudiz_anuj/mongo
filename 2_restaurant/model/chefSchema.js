const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ChefSchema = new Schema({
  _id: { type: Number, required: true },
  sFName: { type: String, required: true },
  sLName: { type: String, required: true },
  nMobile: { type: Number, required: true },
  sUsername: { type: String, required: true },
  sPassword: { type: String, required: true },
  nOrderID: { type: [Number], required: true },
});

module.exports = mongoose.model('Chef', ChefSchema);

//chef needs order details(quantity and orderid) for preparing food
