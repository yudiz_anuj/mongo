const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CustomerSchema = new Schema({
  _id: { type: Number, required: true },
  sFName: { type: String, required: true },
  sLName: { type: String, required: true },
  nMobile: { type: Number, required: true },
  sEmail: { type: String },
  nPaymentID: { type: Number, required: true },
  nFoodID: { type: [Number], required: true },
});

module.exports = mongoose.model('Customer', CustomerSchema);
