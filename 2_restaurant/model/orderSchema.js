const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
  _id: { type: Number, required: true },
  dOrderDate: { type: Date, required: true },
  nQuantity: { type: Number, required: true },
  nCustomerID: { type: Number, required: true },
  nOrderItemID: { type: Number, required: true },
});

module.exports = mongoose.model('Order', OrderSchema);
