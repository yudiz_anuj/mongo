const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OrderItemSchema = new Schema({
  _id: { type: Number, required: true },
  nQuantity: { type: Number, required: true, default: 1 },
  nUnitPrice: { type: Number, required: true },
  nFoodID: { type: [Number], required: true },
});

module.exports = mongoose.model('OrderItem', OrderItemSchema);
