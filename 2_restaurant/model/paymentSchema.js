const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PaymentSchema = new Schema({
  _id: { type: Number, required: true },
  nCustomerID: { type: Number, required: true },
  nOrderID: { type: Number, required: true },
  dPaymentDate: { type: Date, required: true },
  nAmount: { type: Number, required: true },
  sPaymentMethod: { type: String, required: true },
});

module.exports = mongoose.model('Payment', PaymentSchema);
