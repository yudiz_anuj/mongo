const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const FoodItemSchema = new Schema({
  _id: { type: Number, required: true },
  sName: { type: String, required: true },
  nQuantity: { type: Number, required: true, default: 1 },
  nUnitPrice: { type: Number, required: true },
});

module.exports = mongoose.model('FoodItem', FoodItemSchema);
