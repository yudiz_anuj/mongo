const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AdministratorSchema = new Schema({
  _id: { type: Number, required: true },
  sFName: { type: String, required: true },
  sLName: { type: String, required: true },
  sUsername: { type: String, required },
  sPassword: { type: String, required },
  nMenuID: { type: [Number], required },
});

module.exports = mongoose.model('Administrator', AdministratorSchema);

//administrator manages menu
