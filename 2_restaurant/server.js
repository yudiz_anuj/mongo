const express = require('express');
const app = express();

const mongoose = require('mongoose');

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const Order = require('./model/orderSchema');

const db = 'mongodb://localhost:27017/Restaurent';

mongoose
  .connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(
    () => {
      console.log('db is connected');
    },
    (err) => {
      console.log('err', err);
    }
  );

app.post('/save', function (req, res) {
  const order = new Order({
    _id: req.body.n_id,
    //...
  });

  order
    .save()
    .then((data) => {
      res.send('Data inserted');
    })
    .catch((err) => {
      res.send(err);
    });
});

app.listen(3000, () => {
  console.log('server is up');
});
