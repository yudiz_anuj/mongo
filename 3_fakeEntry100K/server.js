const express = require('express');
const app = express();

const mongoose = require('mongoose');

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const BookModel = require('./model/bookSchema');

const db = 'mongodb://localhost:27017/BookCollection';

mongoose
  .connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(
    () => {
      console.log('db is connected');
    },
    (err) => {
      console.log('err', err);
    }
  );

//create
app.post('/save', function (req, res) {
  const book = new BookModel({
    _id: req.body.n_id,
    sTitle: req.body.sTitle,
    sAuthor: req.body.sAuthor,
    sEmail: req.body.sEmail,
    sCategory: req.body.sCategory,
    nPrice: req.body.nPrice,
    dDatePublished: new Date(req.body.dDatePublished),
    aEdition: [...req.body.aEdition],
  });

  book
    .save()
    .then((data) => {
      res.send('Data inserted');
    })
    .catch((err) => {
      res.send(err);
    });
});

//create 100k
app.post('/save100k', async function (req, res) {
  for (let i = 1; i < 100000; i++) {
    const book = new BookModel({
      _id: i,
      sTitle: `myTitle${i}`,
      sAuthor: `myAuthor${i}`,
      sEmail: `emp.${i}@yudiz.in`,
      sCategory: `myCategory${i}`,
      nPrice: 100 + i,
      dDatePublished: new Date(),
      aEdition: [1 + i, 2 + i, 3 + i, 4 + i, 5 + i],
    });
    await book.save(function (err, data) {
      if (err) {
        console.log(err);
      } else {
        console.log('inserted', i);
      }
    });
  }
  res.send('100k data is being inserted...');
});

app.listen(3000, () => {
  console.log('server is up');
});
