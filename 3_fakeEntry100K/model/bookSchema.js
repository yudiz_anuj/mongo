const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BookSchema = new Schema({
  _id: { type: Number, required: true },
  sTitle: { type: String, required: true },
  sAuthor: { type: String, required: true },
  sEmail: { type: String, required: true },
  sCategory: { type: String, required: true },
  nPrice: { type: Number, required: true, default: 100 },
  dDatePublished: { type: Date, required: true },
  aEdition: { type: [Number], required: true },
});

module.exports = mongoose.model('Book', BookSchema);
